package com.example.lesintents;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
    }

    @Override
    public void finish(){
        Intent intent = new Intent();
        EditText editText = findViewById(R.id.editTextMessage);
        intent.putExtra("message",editText.getText().toString());
        setResult(Activity.RESULT_OK,intent);
        super.finish();
    }

    public void retour(View view) {
        finish();
    }
}