package com.example.lesintents.model;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class Contact implements Parcelable {
    private String nom;
    private String prenom;
    private String tel;
    private String web;
    private Bitmap bitmap;

    public Contact(String nom, String prenom, String tel, String web, Bitmap bitmap) {
        this.nom = nom;
        this.prenom = prenom;
        this.tel = tel;
        this.web = web;
        this.bitmap = bitmap;
    }

    protected Contact(Parcel in) {
        nom = in.readString();
        prenom = in.readString();
        tel = in.readString();
        web = in.readString();
        bitmap = in.readParcelable(Bitmap.class.getClassLoader());
    }

    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel in) {
            return new Contact(in);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(nom);
        parcel.writeString(prenom);
        parcel.writeString(tel);
        parcel.writeString(web);
        parcel.writeParcelable(bitmap, i);
    }
}
