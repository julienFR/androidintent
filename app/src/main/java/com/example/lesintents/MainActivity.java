package com.example.lesintents;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lesintents.model.Contact;

public class MainActivity extends AppCompatActivity {
    private Bitmap thumbnail;
    private String tel;
    private String message;
    private EditText edtphone;

    ActivityResultLauncher<Intent> mStartForResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent intent = result.getData();
                        message = intent.getStringExtra("message");
                        edtphone.setText(message);
                    }
                }
            });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = findViewById(R.id.buttonConsulter);
        edtphone = findViewById(R.id.editTextPhone);

    }

    public void consulter(View view) {
        EditText ednom = findViewById(R.id.editTextTextNom);
        String nom = ednom.getText().toString();
        EditText edprenom = findViewById(R.id.editTextPrenom);
        String prenom = edprenom.getText().toString();
        //edtphone = findViewById(R.id.editTextPhone);
        tel = edtphone.getText().toString();
        EditText edtweb = findViewById(R.id.editTextWeb);
        String web = edtweb.getText().toString();
        Intent intent = new Intent(this,ActivityConsult.class);
        /*intent.putExtra("nom",nom);
        intent.putExtra("prenom",prenom);
        intent.putExtra("phone",tel);
        intent.putExtra("web",web);
        intent.putExtra("photo",thumbnail);*/
        Contact contact = new Contact(nom,prenom,tel,web,thumbnail);
        intent.putExtra("contact",contact);
        startActivity(intent);
    }

    public void prendrephoto(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 1);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            this.thumbnail = data.getParcelableExtra("data");
            ImageView imageView = findViewById(R.id.imageViewMain);
            imageView.setImageBitmap(thumbnail);
        }
    }

    public void gotoAct3(View view) {
        mStartForResult.launch(new Intent(this, MainActivity3.class));
    }
}