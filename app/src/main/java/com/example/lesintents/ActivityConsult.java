package com.example.lesintents;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lesintents.model.Contact;

public class ActivityConsult extends AppCompatActivity {
    private String nom;
    private String prenom;
    private String numtel;
    private String web;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consult);
        if (savedInstanceState == null){
            Bundle bundle = getIntent().getExtras();
            /*nom = bundle.getString("nom");
            prenom = bundle.getString("prenom");
            numtel = bundle.getString("phone");
            web = bundle.getString("web");
            Bitmap photo = bundle.getParcelable("photo");*/
            Contact contact = bundle.getParcelable("contact");
            nom = contact.getNom();
            prenom = contact.getPrenom();
            numtel = contact.getTel();
            web = contact.getWeb();
            Bitmap photo = contact.getBitmap();
            TextView tvNom = findViewById(R.id.textViewAffNom);
            TextView tvPrenom = findViewById(R.id.textViewAffPrenom);
            tvNom.setText("Nom : " + nom);
            tvPrenom.setText("Prenom : "+prenom);
            ImageView imageView = findViewById(R.id.imageViewConsult);
            imageView.setImageBitmap(photo);

        }

    }

    public void appeler(View view) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + numtel));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void gotoweb(View view) {
        Uri webpage = Uri.parse("https://"+web);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}